#!/usr/env/bin python

import sys


def get_max(filename, lower, upper):
    f = open(filename, 'r')
    high = 0
    for line in f:
        line = line.rstrip('\r\n')
        gildi = line.split(' ')
        if (float(gildi[0]) > lower) and (float(gildi[0]) < upper):
                if float(gildi[1]) > high:
                    high = float(gildi[1])
    return high

def get_width(filename, lower, upper):
    f = open(filename, 'r')
    maxx = get_max(filename, lower, upper)
    hmax = maxx/2
    hdeg1 = 0.0
    hdeg2 = 0.0
    last = 0.0
    for line in f:
        line = line.rstrip('\r\n')
        gildi = line.split(' ')
        if (float(gildi[0]) > lower) and (float(gildi[0]) < upper):
            if (float(gildi[1]) == maxx):
                hdeg1 = hdeg2
                last = 0
            if (abs(hmax - float(gildi[1])) < abs(hmax - last)):
                hdeg2 = float(gildi[0])
                last = float(gildi[1])
    return hdeg2 - hdeg1

lower = float(sys.argv[2])
upper = float(sys.argv[3])
filename = sys.argv[1]
fwhm = get_width(filename, lower, upper)
print fwhm
