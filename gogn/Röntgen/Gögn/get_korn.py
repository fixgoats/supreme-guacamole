#!/usr/env/bin python

import sys
import math

def get_wasted(theta, beta):
    lambo = 1.5405940*(10**(-10))
    k = 0.9
    theta = math.cos(math.radians(theta/2))
    return k*lambo/(beta*theta)

theta = float(sys.argv[1])
beta = float(sys.argv[2])

print get_wasted(theta, beta)
