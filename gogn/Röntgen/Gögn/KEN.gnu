set encoding utf8
set grid mxtics
set term post eps enhanced color solid "Helvetica" 20
set ylabel "{/Symbol g}/s [s^{-1}]"
set xlabel "2{/Symbol q} [°]"
set mxtics
set title "KEN004 Röntgenvíxlverkun" 
#set yrange [0:30]
#set xrange [0:30]
set logscale y
set output 'KEN004_vixl.eps'
plot 'Ken004_vixl.xy' u 1:2 notitle w l
set title "KEN002 Röntgenvíxlverkun" 
#set yrange [0:30]
#set xrange [0:30]
set logscale y
set output 'KEN002_vixl.eps'
plot 'Ken002_vixl.xy' u 1:2 notitle w l
set title "KEN004 Speglunarmæling" 
#set yrange [0:30]
#set xrange [0:30]
#unset logscale y
set output 'KEN004_speglun.eps'
plot 'Ken004_speglun.xy' u 1:2 notitle w l
set title "KEN002 Speglunarmæling" 
#set yrange [0:30]
#set xrange [0:30]
#set logscale y
set output 'KEN002_speglun_good.eps'
plot 'Ken004_speglun.xy' u 1:2 notitle w l
set title "Speglun KEN004 og KEN002" 
#set yrange [0:30]
#set xrange [0:30]
#set logscale y
set output 'Speglanir.eps'
plot 'Ken004_speglun.xy' u 1:2 w l title "KEN004", 'Ken002_speglun_good.xy' u 1:2 w l title 'KEN002'
